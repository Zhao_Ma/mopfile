
#pragma once
#include "mopItem.h"
#include "compression.h"
// duplicates of each function, with version depending on the compile time flag

#ifdef MOPFILE



/** 
 * compresses the mopitem and prepends compressed portion char length inside |'s
 */
std::string mopToString(mopItem in) {
  std::stringstream tmp;
  std::stringstream write;
  std::string compressed;
  tmp << in.name << ",";
  tmp << in.mass << ",";
  tmp << in.radius << ",";
  tmp << in.size<< ",";
  tmp << in.r << ",";
  tmp << in.g << ",";
  tmp << in.b << ",";
  tmp << in.x << ",";
  tmp << in.y << ",";
  tmp << in.z << ",";
  tmp << in.xv << ",";
  tmp << in.yv << ",";
  tmp << in.zv;
  compressed = compress(tmp.str());
  write << '|';
  write << compressed.length();
  write << '|';
  write << compressed;
  return write.str();
}

mopItem decompressMopItem(std::string in, char delim) {
    mopItem tmp;
    std::string word;
    int pos = 0;
    int size = in.size();
    int i;
    std::string inflated = decompress(in);
    // delimeter is a , usually
    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.name = word;
    word = "";
    
    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.mass = atof (word.c_str());
    word = "";

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.radius = atof (word.c_str());
    word = "";

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.size = atoi (word.c_str());
    word = "";    

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.r = atoi (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.g = atoi (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.b = atoi (word.c_str());
    word = "";      

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.x = atof (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.y = atof (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.z = atof (word.c_str());
    word = "";

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.x = atof (word.c_str());
    word = "";

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.xv = atof (word.c_str());
    word = "";

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.yv = atof (word.c_str());
    word = "";

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.zv = atof (word.c_str());
    //word = "";  		      
    return tmp;
  }





#endif
#ifdef MOPFILELIGHT
/** 
 * compresses the mopitem and prepends compressed portion char length
 */
std::string mopToString(mopItem in) {
    std::stringstream tmp;
    std::stringstream write;
    std::string compressed;
    tmp << in.id << ",";
    tmp << in.size << ",";
    tmp << in.r << ",";
    tmp << in.g << ",";
    tmp << in.b << ",";
    tmp << in.x << ",";
    tmp << in.y << ",";
    tmp << in.z;
    compressed = compress(tmp.str());
    write << '|';
    write << compressed.length();
    write << '|';
    write << compressed;
    return write.str();
}

mopItem decompressMopItem(std::string in, char delim) {
    mopItem tmp;
    std::string word;
    int pos = 0;
    int size = in.size();
    int i;
    std::string inflated = decompress(in);
    // delimeter is a , usually
    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.id = atoi(word.c_str());
    word = "";
    
    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.size = atoi (word.c_str());
    word = "";    

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.r = atoi (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.g = atoi (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.b = atoi (word.c_str());
    word = "";      

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.x = atof (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.y = atof (word.c_str());
    word = "";  

    for (i=pos;i<size;i++) {
      if (in.at(i) == delim) {
	pos++;
	break;
      }
      word.push_back(in.at(i));
      pos++;
    }
    tmp.z = atof (word.c_str());
    word = "";
		      
    return tmp;
  }

#endif

int getCompressedPortionsize(std::string in) {
  std::size_t pos = in.find_first_of ("|",1);
  std::string out = in.substr (1,pos); // start at 1 to skip first '|'
  return atoi(out.c_str());
}


std::string getCompressedPortion(std::string in) {
  std::size_t pos = in.find_first_of ("|",1);
  return in.substr (pos+1);
}
