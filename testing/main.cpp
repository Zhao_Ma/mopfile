/**
 * Author      : Dr Carey Pridgeon
 * Copyright   : Dr Carey Pridgeon 2015
 * Licence     : Licensed under the Apache License, Version 2.0 (the "License");
 *             : you may not use this file except in compliance with the License.
 *             : You may obtain a copy of the License at
 *             : http://www.apache.org/licenses/LICENSE-2.0
 *             :
 *             : Unless required by applicable law or agreed to in writing,
 *             : software distributed under the License is distributed on
 *             : an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 *             : either express or implied. See the License for the specific
 *             : language governing permissions and limitations under the License.
 */
 

#include <iostream>
#include <iomanip> 
#include "../mopFile.h"


int main(int argc, char *argv[]) {

#ifdef MOPFILE
    std::cout<<"> Compiled with full fat MopFile type" <<std::endl;
#endif
    
    
#ifdef MOPFILELIGHT
    std::cout<<"> Compiled with smaller MopFile type" <<std::endl;
#endif 
    int i;
    mopItem item;
    mopState *state = new mopState;
  state->size = 0;
std::cout << ">" <<  std::endl;
std::cout << "> testing output " <<  std::endl;
std::cout << ">" <<  std::endl;
 std::cout << ">" <<  std::endl;

    for (i=0;i<5;i++) {
      item.id = i;
      item.size = 20;
      item.r = 255;
      item.g = 255;
      item.b = 0;
      item.x=i;
      item.y = i+1;
      item.z = i+2;
      addToMopState (state,item);
    }
    std::string tmp;
        std::string tmpu;
    for (i=0;i<5;i++) {
      tmp=mopToString(state->content[i]);
      std::cout << "> compressed mopItem - "<< tmp<< std::endl;
      std::cout << "> compressed portion size - "<< getCompressedPortionsize(tmp)<< std::endl;
      tmpu = getCompressedPortion(tmp);
      std::cout << "> text to decompress - "<< tmpu<< std::endl;     
      std::cout << "> decompressed - "<< decompress(tmpu)<< std::endl<< std::endl;     
    }
    return 0;
}
